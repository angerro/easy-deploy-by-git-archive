@echo off

rem SWITH to encoding = Cyrillic (Windows 866) or use chcp 65001 for UTF-8 !!!
chcp 65001

SetLocal EnableDelayedExpansion

echo Deployer by Angerro: version 1.1
echo Поиск файла с данными последнего задеплоенного коммита...

rem Читаем первую строку файла last_commit в переменную lcommit
set /P lcommit=<last_commit

if "%lcommit%"=="" (
	echo Файл не найден.
	set /P lcommit="Введите SHA последнего задеплоенного коммита: "
	rem echo !lcommit!

	rem Записываем построчно пути файлов, которые были изменены в переменные var{номер строки}
	set count=0
	for /f "tokens=* USEBACKQ" %%F IN (`git diff --name-only !lcommit! HEAD`) do (
		set /a count=!count!+1
		set var!count!=%%F
	)

) else (
	echo Файл найден. SHA коммита = %lcommit%

	rem Записываем построчно пути файлов, которые были изменены в переменные var{номер строки}
	set count=0
	for /f "tokens=* USEBACKQ" %%F IN (`git diff --name-only %lcommit% HEAD`) do (
		set /a count=!count!+1
		set var!count!=%%F
	)
)

rem params - параметры на вход функции git archive (существующие файлы)
set params=

rem Перебираем файлы для формирование списка параметров на вход команды git archive
for /L %%i in (1, 1, !count!) do (
    if exist "!var%%i!" (
        set params=!params! "!var%%i!"
    )
)

rem Фиксируем в файл last_commit
echo Записываю данные последнего коммита в файл last_commit...

rem Фиксируем SHA последнего коммита
for /f "tokens=* USEBACKQ" %%F IN (`git rev-parse HEAD`) do (
	echo %%F>last_commit
)

for /f "tokens=* USEBACKQ" %%F IN (`git rev-parse --abbrev-ref HEAD`) do (
	echo %%F>>last_commit
)

rem здесь %s экранирована в %%s,  = экранирована в ^=
for /f "tokens=* USEBACKQ" %%F IN (`git show --format^=%%s%%n%%cd --quiet`) do (
	echo %%F>>last_commit
)

echo ====================>>last_commit
echo Лог изменений GIT-а:>>last_commit
for /f "tokens=* USEBACKQ" %%F IN (`git diff --name-status !lcommit! HEAD`) do (
	echo %%F>>last_commit
	echo %%F
)
echo ====================>>last_commit

echo Проверяю есть ли добавленные/изменённые файлы для создания архива изменений...

if defined params (
	echo Добавленные/Изменённые файлы ЕСТЬ.
	echo Создаю архив deploy.zip с данными для деплоя...
	call git archive -o deploy.zip HEAD !params!
	echo Создание завершено.
	echo Проверьте last_commit на предмет переименованных/удалённых файлов.
) else (
	echo Добавленных/Изменённых файлов НЕТ. Проверьте last_commit на предмет переименованных/удалённых файлов.
)

pause